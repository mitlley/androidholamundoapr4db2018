package test.inacap.a2018apr4dbhelloworld.controlador;

import android.content.ContentValues;
import android.content.Context;

import test.inacap.a2018apr4dbhelloworld.modelo.MainDBContract;
import test.inacap.a2018apr4dbhelloworld.modelo.UsuariosModel;

public class UsuariosController {
    private UsuariosModel capaModelo;

    public UsuariosController(Context ctx){
        this.capaModelo =  new UsuariosModel(ctx);
    }

    public void crearUsuario(String uName, String p1, String p2) throws Exception{
        // Validar los datos
        if(!p1.equals(p2)){
            // Pass no coinciden
            // Lanzar el Exception
            throw new Exception("Las contraseñas no coinciden.");
        }

        // Crear el usuario
        ContentValues usuario = new ContentValues();
        usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_USERNAME, uName);
        usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD, p1);

        this.capaModelo.crearUsuario(usuario);

    }

    public int login(String username, String password){
        // Devolvemos 0 cuando inicio sesion
        // Pedir el usuario a la bd
        ContentValues usuario = this.capaModelo.obtenerUsuarioPorUsername(username);

        // Validamos que el usuario existe
        if(usuario == null){
            // El usuario no existe
            return 2;
        }

        // Validamos las passwords
        // Password guardada en la BD
        String passDB = usuario.getAsString(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD);
        if(password.equals(passDB)){
            // Inicio exitoso
            return 0;
        }
        return 1;

    }
}
