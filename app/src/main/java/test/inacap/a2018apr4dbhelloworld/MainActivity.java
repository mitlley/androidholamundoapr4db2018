package test.inacap.a2018apr4dbhelloworld;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import test.inacap.a2018apr4dbhelloworld.controlador.UsuariosController;

public class MainActivity extends AppCompatActivity {


    public TextView tvNombreUsuario, tvRegistrar;
    public EditText etNombreUsuario, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Validar si la persona ya habia iniciado sesion
        SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
        boolean yaSeLogueo = sesiones.getBoolean("estaLogueado", false);
        if(yaSeLogueo){
            Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
            startActivity(i);
            finish();
        }

        this.etNombreUsuario = findViewById(R.id.etNombreUsuario);
        this.etPassword = findViewById(R.id.etContrasena);

        this.tvRegistrar = findViewById(R.id.tvRegistrar);

        this.tvNombreUsuario = findViewById(R.id.tvNombreUsuario);
        this.tvNombreUsuario.setVisibility(View.INVISIBLE);

        Button btIngresar = findViewById(R.id.btIngresar);
        btIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Capturar y mostrar los datos
                String nombreUsuario = etNombreUsuario.getText().toString();
                String password = etPassword.getText().toString();

                UsuariosController controller = new UsuariosController(getApplicationContext());
                int inicioSesion = controller.login(nombreUsuario, password);

                if(inicioSesion == 0){
                    // Inicio sesion
                    SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sesiones.edit();
                    editor.putBoolean("estaLogueado", true);
                    editor.commit();

                    Intent intent = new Intent(MainActivity.this, HomeDrawerActivity.class);
                    startActivity(intent);
                    finish(); // Cerrar esta ventana
                }
            }
        });

        this.tvRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Intent
                Intent intentNuevaVentana = new Intent(MainActivity.this, RegistrarUsuarioActivity.class);
                startActivity(intentNuevaVentana);
            }
        });
    }
}
